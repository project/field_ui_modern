Modern Field UI


Currently, the app must be running locally. Run `yarn start` to start the app on port
3000. Then, head to admin/field_ui_plus

*** At the moment you'll need to apply several patches to jsonapi_schema ***
From the following issues:
https://www.drupal.org/project/jsonapi_schema/issues/3324769
https://www.drupal.org/project/jsonapi_schema/issues/3324824
https://www.drupal.org/project/jsonapi_schema/issues/3324982
