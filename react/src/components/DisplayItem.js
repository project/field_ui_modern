import {useState} from "react";
import Box from '@mui/material/Box';
import Button from "@mui/material/Button";
import SubformWrapper from "./SubformWrapper";
import EditDisplayForm from './subforms/EditDisplayForm';

const DisplayItem = ({field}) => {
  const [uiMode, setUiMode] = useState(null);

  return (
      <div style={{marginLeft: '1rem', marginBottom: '1rem'}}>
        {field}
        <Button size='sm' onClick={() => setUiMode('edit')}>edit</Button>
          {uiMode !== null &&  <Box sx={{  p: 2, border: '1px solid grey' }}>
            <SubformWrapper setUiMode={setUiMode}><EditDisplayForm/></SubformWrapper>
          </Box>}
      </div>

  );
};

export default DisplayItem;
