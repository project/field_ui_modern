import TreeItem from "@mui/lab/TreeItem";
import {useState} from "react";
import Box from '@mui/material/Box';
import Button from "@mui/material/Button";
import SubformWrapper from "./SubformWrapper";
import EditFieldForm from "./subforms/EditFieldForm";
import EditFieldStorageForm from "./subforms/EditFieldStorageForm";

const FieldItem = ({
                     parentIndex,
                     fieldIndex,
                     field
                   }) => {
  const [uiMode, setUiMode] = useState(null);
  const getForms = () => {
    switch (uiMode) {
      case 'edit':
        return <EditFieldForm field={field} />
      case 'storage':
        return <EditFieldStorageForm field={field} />
      case 'delete':
        return 'Are you sure you wanna delete'
      default:
        return ''
    }
  }



  return (
    <div style={{marginLeft: '1rem'}}>
      {field.attributes.label}
      <Button size='sm' onClick={() => setUiMode('edit')}>Edit</Button>
      <Button size='sm' onClick={() => setUiMode('storage')}>Storage Settings</Button>
      <Button size='sm' onClick={() => setUiMode('delete')}>Delete</Button>
      {uiMode !== null &&  <Box sx={{  p: 2, border: '1px solid grey' }}>
         <SubformWrapper setUiMode={setUiMode}>{getForms()}</SubformWrapper>
      </Box>}
    </div>

  );
};

export default FieldItem;
