import TreeView from "@mui/lab/TreeView";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import TreeItem from "@mui/lab/TreeItem";
import Button from "@mui/material/Button";
import {useEffect, useState} from "react";
import ManageDisplay from "./ManageDisplay";
import ManageFormDisplay from "./ManageFormDisplay";
import Fields from "./Fields";
import SubformWrapper from "./SubformWrapper";
import AddContentTypeForm from "./subforms/AddContentTypeForm";
import EditContentTypeForm from "./subforms/EditContentTypeForm";
import Box from "@mui/material/Box";
import { useSelector, useDispatch } from 'react-redux'
import {set as contentTypesSet, remove as contentTypesRemove} from '../stores/contentTypeSlice'
import {set as fieldConfigSet} from '../stores/fieldSlice'
import {set as fieldRelatedSet} from '../stores/fieldRelatedSlice'
import DeleteContentTypeForm from "./subforms/DeleteContentTypeForm";


const FieldUiRoot = () => {
  const [loading, setLoading] = useState(true);
  const [uiMode, setUiMode] = useState(null);

  const contentTypes = useSelector(state => state.contentTypes.value)

  const dispatch = useDispatch()

  useEffect(() => {
    Promise.all([
      '../jsonapi/node_type/node_type/',
      'field_ui_modern/all_data'
    ].map(url =>
      fetch(url).then(resp => resp.json())
    )).then(async jsons => {
      const contentTypeJson = jsons[0]
      const bundleFields = {};
      for (const contentType of contentTypeJson.data) {
        const contentTypeResponse = await fetch(`../jsonapi/field_config/field_config/?filter[bundle]=${contentType.attributes.drupal_internal__type}`);
        bundleFields[contentType.attributes.drupal_internal__type] = await contentTypeResponse.json();
      }
      dispatch(fieldConfigSet(bundleFields));
      dispatch(contentTypesSet(contentTypeJson));
      dispatch(fieldRelatedSet(jsons[1]))
      setLoading(false)
    })
  }, [])

  const removeContentType = (contentType) => {
    dispatch(contentTypesRemove(contentType))
  }


  const getForms = (contentType) => {
    switch (uiMode) {
      case 'edit':
        return <SubformWrapper
          setUiMode={setUiMode}>
          <EditContentTypeForm contentType={contentType} />
        </SubformWrapper>
      case 'delete':
        return <SubformWrapper submitHandler={() => removeContentType(contentType)} setUiMode={setUiMode}>
          <DeleteContentTypeForm contentType={contentType} />
        </SubformWrapper>
      default:
        return ''
    }
  }

  const label = (contentType) => {
    return <>
      {contentType.attributes.name}
      <Button size='sm' onClick={() => setUiMode('edit')}>Edit</Button>
      <Button size='sm' onClick={() => setUiMode('delete')}>Delete</Button>
    </>
  }

  const mainForm = () => (<>
    <TreeView
      aria-label="content types"
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      sx={{ overflowY: "auto" }}
    >
      {contentTypes.data.map((contentType, index) => (
        <TreeItem key={index} nodeId={`${index}`} label={label(contentType)}>
          {uiMode !== null &&  <Box sx={{  p: 2, border: '1px solid grey' }}>
            {getForms(contentType)}
          </Box>}
          <Fields
            parentIndex={index}
            contentType={contentType}
          />
          <ManageDisplay parentIndex={index} contentType={contentType} />
          <ManageFormDisplay parentIndex={index} contentType={contentType} />
        </TreeItem>
      ))}
    </TreeView>
    <Button onClick={() => setUiMode('add_content_type')} size="small" variant="contained">
      + Add Content Type
    </Button>
    {uiMode === 'add_content_type' && <SubformWrapper setUiMode={setUiMode}><AddContentTypeForm/></SubformWrapper> }

  </>)


  return (
    <>
      {loading === true && <p>Loading</p>}
      {loading === false && mainForm()}
    </>
  );
};

export default FieldUiRoot;
