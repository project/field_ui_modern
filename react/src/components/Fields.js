import FieldItem from "./FieldItem";
import TreeItem from "@mui/lab/TreeItem";
import SubformWrapper from "./SubformWrapper";
import {useState} from "react";
import AddFieldForm from "./subforms/AddFieldForm";
import { useSelector, useDispatch } from 'react-redux'

const Fields = (props) => {
  const { parentIndex, contentType } = props;
  const fieldState = useSelector(state => state.fields.value)
  const fieldRelatedData = useSelector(state => state.fieldRelatedData.value);
  const {field_types} = fieldRelatedData;

  const fieldList = fieldState[contentType.attributes.drupal_internal__type];
  const [uiMode, setUiMode] = useState(null);

  const addField = (e) => {
    setUiMode('addField');
    e.stopPropagation()
  }


  const label = () => {
    return <>
      <b>Fields</b> <button disabled={uiMode} onClick={addField}>Add field</button>
    </>
  }
  return (
    <>
      {uiMode !== null &&
        <SubformWrapper setUiMode={setUiMode}>
          <AddFieldForm
            {...props}
          />
        </SubformWrapper>}
      <TreeItem nodeId={`${parentIndex}-fields`} label={label()}>

        {fieldList.data.map((field, index) => (
        field_types[field.attributes.field_type].no_ui === false &&
        <FieldItem
          parentIndex={parentIndex}
          fieldIndex={index}
          key={index}
          field={field}
        />
      ))}
    </TreeItem>
    </>
  );
};

export default Fields;
