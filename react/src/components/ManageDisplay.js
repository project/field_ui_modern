import TreeItem from "@mui/lab/TreeItem";
import DisplayItem from './DisplayItem';
import {useEffect, useState} from 'react';
import {InputLabel, Select} from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import {useSelector} from "react-redux";

const ManageDisplay = (props) => {
  const { parentIndex, contentType } = props;
  const [viewModesList, setViewModesList] = useState([]);
  const [currentMode, setCurrentMode] = useState('default');
  const [displayList, setDisplayList] = useState(null);

  const contentTypeName = contentType.attributes.drupal_internal__type;
  const fieldData = useSelector(state => state.fields.value)
  const fieldRelatedData = useSelector(state => state.fieldRelatedData.value)
  useEffect(() => {
    fetch(`../jsonapi/entity_view_display/entity_view_display/?filter[bundle]=${contentTypeName}`)
      .then((res) => res.json())
      .then((displays) => {
        if (displays.data && displays.data.length) {
          const viewModes = displays.data.map((item) => item.attributes.mode)
          const displayList = displays.data.find((item) => item.attributes.mode === currentMode);
          setViewModesList(viewModes);
          setDisplayList(displayList.attributes)
        }
      })
  }, [currentMode]);



  return  (<TreeItem nodeId={`${parentIndex}-display`} label='Display'>
    <FormControl fullWidth style={{marginTop: '1rem'}}>
      <InputLabel id="demo-simple-select-label">- Manage display for a view mode -</InputLabel>
      <Select onChange={(e) => setCurrentMode(e.target.value)} label="Change view mode setting" value={currentMode} id="group">
        {viewModesList.map((mode, index) => {
          return <MenuItem key={index} value={mode}>{mode}</MenuItem>
        })
        }
      </Select>
    </FormControl>
    {displayList && Object.keys(displayList.content).map((field, index) => (
        <DisplayItem
            parentIndex={parentIndex}
            fieldIndex={index}
            key={index}
            field={field}
        />
    ))}
    <div>Disabled</div>
    {/* @todo I believe these are fields that should not be displayed at all, they exist
        as fields for functionality purposes but are hidden as they only serve a purpose
        behind the scenes. This will need to instead list the actual "disabled" fields */}
    {displayList && Object.keys(displayList.hidden).map((field, index) => (
        <DisplayItem
            parentIndex={parentIndex}
            fieldIndex={index}
            key={index}
            field={field}
        />
    ))}
  <details><summary>Data about all fields (including if they are no-ui!)</summary>
    <pre>{JSON.stringify(fieldData, null, 2)}</pre>

  </details>
  <details><summary>Field *related* data JSON (field types, formatters, widgets)</summary>
    <pre>{JSON.stringify(fieldRelatedData, null, 2)}</pre>

  </details>
  </TreeItem>)
}

export default ManageDisplay;
