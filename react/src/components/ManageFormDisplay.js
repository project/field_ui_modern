import TreeItem from "@mui/lab/TreeItem";
import SubformWrapper from "./SubformWrapper";
import FormDisplayForm from "./subforms/FormDisplayForm";

const ManageFormDisplay = ({parentIndex, contentType}) => {
  return <TreeItem nodeId={`${parentIndex}-form-display`} label='Form Display'>
    <SubformWrapper><FormDisplayForm contentType={contentType} /></SubformWrapper>
  </TreeItem>
}

export default ManageFormDisplay;
