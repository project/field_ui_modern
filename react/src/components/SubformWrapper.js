import Box from '@mui/material/Box';
import Button from "@mui/material/Button";
import React from 'react';
import {useState} from 'react';

const SubformWrapper = (props) => {
  const {children, setUiMode} = props;
  let {submitHandler} = props;
  if (typeof submitHandler === 'undefined') {
    submitHandler = () => {
      console.warn('this needs an submitHandler to work!')
    }
  }
  const [canSubmit, setCanSubmit] = useState(false)
  const cancel = (e) => {
    e.stopPropagation();
    setUiMode(null);
  }


  return (<Box sx={{p:2}}>
    {React.cloneElement(children, {setCanSubmit, canSubmit})}
    <Button
      disabled={!canSubmit}
      sx={{display: `${canSubmit ? 'inline' : 'none'}`}}
      color="success"
      size='sm'
      variant='contained'
      onClick={submitHandler}
    >
          Submit
    </Button>
    <Button color="error" size='sm' onClick={cancel}>Cancel</Button>
  </Box>)
}

export default SubformWrapper;
