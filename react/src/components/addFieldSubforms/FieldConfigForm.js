import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import {handleChange} from "../subforms/util";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import {useSelector} from "react-redux";

const FieldConfigForm = ({storageFormState, setStorageFormState, fieldFormState, setFieldFormState, schemaFieldJson}) => {
  const {field_types, data_types} = useSelector(state => state.fieldRelatedData.value)
  const {field_storage_config_type} = storageFormState;
  const field_type_info = field_types[field_storage_config_type];

  const config_data_mapping = data_types.field_config_base.mapping;

  const formElements = []
  formElements.push(<Stack key='the-label'><label>{config_data_mapping.label.label}</label><TextField
    variant="outlined"
    value={storageFormState.label || ''}
    onInput={(e) => handleChange(e, 'label', setStorageFormState)}
  /></Stack>)

  const makeInputs = (type, id, label, indx, prefix = '') => {
    const index = prefix + indx;
    if(type === 'boolean') {
      formElements.push(
        <FormControlLabel
          key={index}
          label={label}
          control={<Checkbox
            variant="outlined"
            checked={storageFormState[id] || false}
            onChange={(e) => handleChange(e, id, setFieldFormState, 'checked')}
          />}
        />
      )
    } else {
      if (type === 'integer') {
        formElements.push(
          <Stack key={index} ><label>{label}</label><input
            type='number'
            value={storageFormState[id] || ''}
            onInput={(e) => handleChange(e, id, setFieldFormState)}
          /></Stack>)
      } else if (type === 'text') {
        formElements.push(<Stack key={index}><label>{label}</label><TextField
          key={index}
          variant="outlined"
          value={storageFormState[id] || ''}
          onInput={(e) => handleChange(e, id, setFieldFormState)}
          multiline
          rows={4}
        /></Stack>)
      } else {
        formElements.push(
          <Stack key={index}><label>{label}</label><TextField
            key={index}
            variant="outlined"
            value={storageFormState[id] || ''}
            onInput={(e) => handleChange(e, id, setFieldFormState)}
          /></Stack>)
      }
    }
  }
  const skip = ['label', 'default_value', 'langcode', 'status', 'dependencies', 'drupal_internal__id', 'bundle', 'default_value_callback', 'field_type', 'entity_type', 'field_name']

  Object.entries(schemaFieldJson.definitions.attributes.properties).forEach(([id, info], index) => {
    if (!skip.includes(id) && config_data_mapping.hasOwnProperty(id)) {
      const {type, label} = config_data_mapping[id]
      makeInputs(type, id, label, index, 'base-')
    }

    // if(type === 'boolean') {
    //   formElements.push(
    //     <FormControlLabel
    //       key={index}
    //       label={label}
    //       control={<Checkbox
    //         variant="outlined"
    //         checked={storageFormState[id] || false}
    //         onChange={(e) => handleChange(e, id, setFieldFormState, 'checked')}
    //       />}
    //     />
    //   )
    // } else {
    //   if (info.type === 'integer') {
    //     formElements.push(
    //       <Stack key={index} ><label>{label}</label><input
    //         type='number'
    //         value={storageFormState[id] || ''}
    //         onInput={(e) => handleChange(e, id, setFieldFormState)}
    //       /></Stack>)
    //   } else {
    //     formElements.push(
    //       <Stack key={index}><label>{label}</label><TextField
    //         key={index}
    //         variant="outlined"
    //         value={storageFormState[id] || ''}
    //         onInput={(e) => handleChange(e, id, setFieldFormState)}
    //       /></Stack>)
    //   }
    //
    // }
  })

  const hasSettings = data_types.hasOwnProperty(`field.field_settings.${field_storage_config_type}`) &&
    data_types[`field.field_settings.${field_storage_config_type}`].hasOwnProperty('mapping');
  if (hasSettings) {
    const settings = hasSettings ? data_types[`field.field_settings.${field_storage_config_type}`].mapping : [];
    const default_settings = hasSettings ? field_type_info.default_field_settings : {};
    if (!Object.keys(default_settings).every(property => fieldFormState.hasOwnProperty(property))) {
      // Wrap in a timeout to avoid a nested rendering error. Going with this hacky approach
      // since it is an MVP.
      setTimeout(() => {
        setStorageFormState(previous => ({...previous, ...default_settings}))
      })
      return [];
    }
    Object.entries(settings).forEach(([id,info],index) => {
      const {type, label} = info;
      makeInputs(type, id, label, index, 'settings-')
    })
  }
  return formElements
}

export default FieldConfigForm;
