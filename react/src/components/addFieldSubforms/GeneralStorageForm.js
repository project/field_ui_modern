import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Switch from "@mui/material/Switch";
import {getExistingSelectableFields, getFieldTypesByCategory, handleChange} from "../subforms/util";
import Box from "@mui/material/Box";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {useSelector} from "react-redux";
import Grid from "@mui/material/Grid";


const GeneralStorageForm = ({generalStorageCommitted, setGeneralStorageCommitted, storageFormState, setStorageFormState}) => {
  const existingFields = getExistingSelectableFields(useSelector(state => state.contentTypes.value), useSelector(state => state.fields.value));
  const {field_types} = useSelector(state => state.fieldRelatedData.value)

  if (generalStorageCommitted) {
    return <Box sx={{ border: 2 , borderRadius: 1,  p: 2, bgcolor: '#f2f2f2', display: 'flex', mb:2}}>
      <Typography component='h2' variant='h3' sx={{margin: 0}}>{storageFormState.label}</Typography> <Typography sx={{ml: 2}}>({'field_' + storageFormState.label.toLowerCase().replaceAll("/[.-]/g", '_').replaceAll(' ', '_')}) <i>{storageFormState.field_storage_config_type}</i></Typography>
    </Box>
  }

  const fieldTypesByCategory = getFieldTypesByCategory(field_types);
  const showLabel = (!!storageFormState.existingField.length && !!storageFormState.addExisting) ||  (!!storageFormState.field_storage_config_type.length && !storageFormState.addExisting)
  const canCommitGeneralStorage = showLabel && !!storageFormState.label.length;



  return <>
    <Stack direction="row" spacing={1} alignItems="center">
    <Typography>Add a new field</Typography>
    <Switch disabled={generalStorageCommitted} checked={storageFormState.addExisting} onChange={(e) => handleChange(e, 'addExisting', setStorageFormState, 'checked')} />
    <Typography>Add an existing field</Typography>
  </Stack>
  {/* Dropdowns */}
  <Stack direction="row" spacing={2}>
    <Box hidden={storageFormState.addExisting}>
      <Stack>
        <label >New Field Type</label>
        <Select
          native
          value={storageFormState.field_storage_config_type}
          onChange={(e) => handleChange(e, 'field_storage_config_type', setStorageFormState)}
          size='sm'
          sx={{width: 'auto'}}
          disabled={generalStorageCommitted}
        >
          <option value="">- Select a field type -</option>
          {Object.entries(fieldTypesByCategory).map(([category, group], index) => <optgroup key={index} label={category}>{group.map((item, index) => <option key={index} value={item.id}>{item.label}</option>)}</optgroup>)}
        </Select>
      </Stack>
    </Box>


    <Box hidden={!storageFormState.addExisting}>
      <Stack>

        <label>Choose Existing Field</label>
        <Select
          native
          value={storageFormState.existingField}
          onChange={(e) => handleChange(e, 'existingField', setStorageFormState)}
          size='sm'
          sx={{width: 'auto'}}
          disabled={generalStorageCommitted}
        >
          <option value="">- Select an existing field -</option>
          {[...existingFields].map((existingField, index) => <option key={index} value={existingField.split(':')[1].trim()}>{existingField}</option>)}
        </Select>

      </Stack>
    </Box>
    <Box hidden={!showLabel}>

      <Stack>

        <label>Label</label>

        <TextField
          variant="outlined"
          value={storageFormState.label}
          onInput={(e) => handleChange(e, 'label', setStorageFormState)}
          disabled={generalStorageCommitted}
        />
        <pre>{storageFormState.label.length > 0 && 'field_' + storageFormState.label.toLowerCase().replaceAll("/[.-]/g", '_').replaceAll(' ', '_')}</pre>
      </Stack>
    </Box>
  </Stack>

  {/* commit storage button  */}
  <Box hidden={!canCommitGeneralStorage}>
    <Stack>
      {generalStorageCommitted ?  '' : <Grid item xs={12} md={4}><Button size="small" variant="contained" onClick={() => setGeneralStorageCommitted(true)}>Commit Storage</Button></Grid>}
    </Stack>
  </Box>
  </>
}

export default GeneralStorageForm;
