import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import {handleChange} from "../subforms/util";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import {useSelector} from "react-redux";
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';



const SpecificStorageForm = ({storageFormState, setStorageFormState, specificStorageCommitted, setSpecificStorageCommitted, specificStorageCommittedHandler}) => {
  const {field_types, data_types} = useSelector(state => state.fieldRelatedData.value)
  const {field_storage_config_type} = storageFormState;
  const field_type_info = field_types[field_storage_config_type];
  const default_values = field_type_info.default_storage_settings;
  const hasMapping = data_types.hasOwnProperty(`field.storage_settings.${field_storage_config_type}`) &&
    data_types[`field.storage_settings.${field_storage_config_type}`].hasOwnProperty('mapping');
  const fieldSpecificForm = () => {

    if (hasMapping) {
      // The datatype specific fields are not yet in the form state, we need add them
      // once so the corresponding form elements values are bound to them.
      if (!Object.keys(default_values).every(property => storageFormState.hasOwnProperty(property))) {
        // Wrap in a timeout to avoid a nested rendering error. Going with this hacky approach
        // since it is an MVP.
        setTimeout(() => {
          setStorageFormState(previous => ({...previous, ...default_values}))
        })
        return [];
      }
      const field_map = data_types[`field.storage_settings.${field_storage_config_type}`].mapping;
      const formElements = [];
      Object.entries(field_map).forEach(([id, info], index) => {
        const type = default_values.hasOwnProperty(id) ? typeof default_values[id] : 'unknown';
        if(type === 'boolean') {
          formElements.push(<Grid key={index} item xs={12} md={4}>
            <FormControlLabel
              label={info.label}
              control={<Checkbox
                variant="outlined"
                checked={storageFormState[id] || false}
                onChange={(e) => handleChange(e, id, setStorageFormState, 'checked')}
                disabled={specificStorageCommitted}
              />}
            />
          </Grid>)
        } else {
          if (info.type === 'integer') {
            formElements.push(<Grid key={index} item xs={12} md={4}>
              <Stack><label>{info.label}</label><input
              type='number'
              value={storageFormState[id] || ''}
              onInput={(e) => handleChange(e, id, setStorageFormState)}
              disabled={specificStorageCommitted}
            /></Stack></Grid>)
          } else {
            formElements.push(<Grid key={index} item xs={12} md={4}>
              <Stack><label>{info.label}</label><TextField
              key={index}
              variant="outlined"
              value={storageFormState[id] || ''}
              onInput={(e) => handleChange(e, id, setStorageFormState)}
              disabled={specificStorageCommitted}
              /></Stack></Grid>)
          }

        }
      });
      formElements.push(<Grid key='last' item xs={12}><Button size="small" variant="contained" onClick={specificStorageCommittedHandler}>Commit Storage</Button></Grid>)
      return <Grid container spacing={2}>{formElements}</Grid>;

    } else {
      setSpecificStorageCommitted(true);
    }
  }

  if (specificStorageCommitted) {
    if (hasMapping) {
      const field_map = data_types[`field.storage_settings.${field_storage_config_type}`].mapping;
      return <Box sx={{ border: 2 , p: 2, bgcolor: '#f2f2f2', borderRadius: 1, mb: 2 }}>
        {Object.keys(default_values).map((id, index) => {
          const label = field_map.hasOwnProperty(id) ? field_map[id].label : id;
          const value = storageFormState[id];
          return <span key={index}>
            <Typography style={{display: 'inline-block'}} sx={{fontWeight: 'bold'}}>{label}: </Typography>
            <Typography style={{display: 'inline-block'}} sx={{mx: 2}}>{`${value}`}</Typography>
          </span>
        })}
      </Box>
    }
    return []
  }



  return fieldSpecificForm()
}
export default SpecificStorageForm;
