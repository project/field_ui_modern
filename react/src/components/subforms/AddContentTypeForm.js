import {useEffect, useState} from "react";
import TextField from "@mui/material/TextField";
import {handleChange} from "./util";
import Box from "@mui/material/Box";


const AddContentTypeForm = ({setCanSubmit, canSubmit}) => {
  const [schemaJson, setSchemaJson] = useState(null);
  const [formState, setFormState] = useState({});

  useEffect(() => {
    fetch(`../jsonapi/node_type/node_type/resource/schema`)
      .then(res => res.json())
      .then(json => {
        const tempFormState = {};
        Object.keys(json.definitions.attributes.properties).forEach((property) => {
          tempFormState[property] = '';
        })
        setFormState(tempFormState);
        setSchemaJson(json);
      })
  }, [])
  const formStateReady = Object.keys(formState).length > 0;
  if (formStateReady) {
    if (!!formState.name.length  && !!formState.description.length && !canSubmit) {
      setCanSubmit(true)
    }
    if (canSubmit && (!formState.name.length || !formState.description.length)) {
      setCanSubmit(false)
    }
  }

  return formStateReady && <>

    <Box>
      <label>Name</label>

      <TextField
        variant="outlined"
        value={formState.name}
        onInput={(e) => handleChange(e, 'name', setFormState)}
      />
    </Box>
    <Box>
      <label>Description</label>
      <TextField
        variant="outlined"
        value={formState.description}
        onInput={(e) => handleChange(e, 'description', setFormState)}
        multiline
        rows={4}
      />
    </Box>


    <details>
      <summary>Schema Json</summary>
    {schemaJson !==  null && <pre>{JSON.stringify(schemaJson, null, 2)}</pre>}
    </details>
  </>
}

export default AddContentTypeForm;
