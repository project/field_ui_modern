import Box from '@mui/material/Box';
import GeneralStorageForm from "../addFieldSubforms/GeneralStorageForm";
import SpecificStorageForm from "../addFieldSubforms/SpecificStorageForm";
import FieldConfigForm from "../addFieldSubforms/FieldConfigForm";
import {useSelector} from "react-redux";
import {useState, useEffect} from 'react';

const AddFieldForm =({parentIndex, fieldIndex, setCanSubmit}) => {
  const [schemaStorageJson, setSchemaStorageJson] = useState(null);
  const [schemaFieldJson, setSchemaFieldJson] = useState(null);

  const [generalStorageCommitted, setGeneralStorageCommitted] = useState(false);
  const [specificStorageCommitted, setSpecificStorageCommitted] = useState(false);
  // TO GO BACK TO DEFAULT, BRING BACK 👆the above two lines.
  // TEMP 👇 TO PREFILL STORAGE
  // const [generalStorageCommitted, setGeneralStorageCommitted] = useState(true);
  // const [specificStorageCommitted, setSpecificStorageCommitted] = useState(true);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    Promise.all(['../jsonapi/field_storage_config/field_storage_config/resource/schema', '../jsonapi/field_config/field_config/resource/schema'].map(url =>
      fetch(url).then(resp => resp.json())
    )).then(jsons => {
      setSchemaStorageJson(jsons[0])
      setSchemaFieldJson(jsons[1])
    })
  }, [])

  const [storageFormState, setStorageFormState] = useState({
    field_storage_config_type: '',
    existingField: '',
    addExisting: false,
    label: '',
  });
  // TO GO BACK TO DEFAULT, BRING BACK 👆the setState above.
  // TEMP 👇 TO MAKE STRING
  // const [storageFormState, setStorageFormState] = useState({
  //   existingField: '',
  //   addExisting: false,
  //   case_sensitive: false,
  //   field_storage_config_type: 'string',
  //   label: 'Cool String',
  //   max_length: 255,
  // });

// TEMP 👇 TO MAKE INT
//   const [storageFormState, setStorageFormState] = useState( {
//     addExisting: false,
//     case_sensitive: false,
//     field_storage_config_type: 'integer',
//     size: 'normal',
//     unsigned: false,
//     label: 'Fine Integer',
//   });

  const [fieldFormState, setFieldFormState] = useState({});
  const { field_types } = useSelector(state => state.fieldRelatedData.value)


  const specificStorageCommittedHandler = () => {
    setSpecificStorageCommitted(true)
    setCanSubmit(true)
    setFieldFormState((prev) => ({
      ...prev,
      label: storageFormState.label,
      dependencies: !storageFormState.addExisting ?
        {
          use: 'parentIndex and fieldIndex once this is integrated'
        } :
        {
          todo: 'existing fields'
        }
    }))
  }

  useEffect(() => {
    Promise.all([
      '../jsonapi/field_storage_config/field_storage_config/resource/schema',
      '../jsonapi/field_config/field_config/resource/schema',
    ].map(url =>
      fetch(url).then(resp => resp.json())
    )).then(jsons => {
      setSchemaStorageJson(jsons[0])
      setSchemaFieldJson(jsons[1])
      const tempStorageFormState = {};
      Object.keys(jsons[0].definitions.attributes.properties).forEach((property) => {
        tempStorageFormState[property] = '';
      })
      setStorageFormState((prev) => ({...tempStorageFormState, ...prev}))
      const tempFieldFormState = {}
      Object.keys(jsons[1].definitions.attributes.properties).forEach((property) => {
        tempFieldFormState[property] = '';
      })
      setFieldFormState((prev) => ({...tempFieldFormState, ...prev}))
      setLoading(false)
    })
  }, [])
  //
  if (specificStorageCommitted &&
    !Object.keys(field_types[storageFormState.field_storage_config_type].default_field_settings).every(property => fieldFormState.hasOwnProperty(property))
  ) {

    setFieldFormState(prev => ({...field_types[storageFormState.field_storage_config_type].default_field_settings, ...prev}))
  }


  return(
   loading === false && <>
     <GeneralStorageForm
       generalStorageCommitted={generalStorageCommitted}
       setGeneralStorageCommitted={setGeneralStorageCommitted}
       storageFormState={storageFormState}
       setStorageFormState={setStorageFormState} />

     {/* field specific storage form */}
     <Box hidden={!generalStorageCommitted} >
       {generalStorageCommitted === true &&
           <SpecificStorageForm
             storageFormState={storageFormState}
             setStorageFormState={setStorageFormState}
             specificStorageCommitted={specificStorageCommitted}
             setSpecificStorageCommitted={setSpecificStorageCommitted}
             specificStorageCommittedHandler={specificStorageCommittedHandler}
           />
       }


     </Box>
     <Box hidden={!specificStorageCommitted} >
       <FieldConfigForm
         storageFormState={storageFormState}
         setStorageFormState={setStorageFormState}
         fieldFormState={fieldFormState}
         setFieldFormState={setFieldFormState}
         schemaFieldJson={schemaFieldJson}
       />
     </Box>
     {/*  Uncomment below to get schema details  */}
     {/*<details>*/}
     {/*  <summary>Storage Schema JSON</summary>*/}
     {/*  <pre>{JSON.stringify(schemaStorageJson, null, 2)}</pre>*/}
     {/*</details>*/}
     {/*<details>*/}
     {/*  <summary>Field Schema JSON</summary>*/}
     {/*  <pre>{JSON.stringify(schemaFieldJson, null, 2)}</pre>*/}
     {/*</details>*/}

   </>

 )
 }

export default AddFieldForm;
