import {useState, useEffect} from 'react';
import Typography from "@mui/material/Typography";

const DeleteContentTypeForm = (props) => {
  const {setCanSubmit, contentType} = props;
  useEffect(() => {
    setCanSubmit(prev => true);
  }, [])

  return <Typography>{`This will delete the ${contentType.attributes.name} content type. you sure?`}</Typography>
}
export default DeleteContentTypeForm;


