import {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {handleChange} from "./util";


const EditContentTypeForm = ({contentType, canSubmit, setCanSubmit}) => {
  const [schemaJson, setSchemaJson] = useState(null);
  const [formState, setFormState] = useState({});

  useEffect(() => {
    fetch(`../jsonapi/node_type/node_type/?filter[drupal_internal__type]=${contentType.attributes.drupal_internal__type}`)
      .then(res => res.json())
      .then(json => {
        const tempFormState = json.data[0].attributes;
        tempFormState.id = json.data[0].id;
        setFormState(tempFormState);
        setSchemaJson(json)
      })
  }, [])

  const formStateReady = Object.keys(formState).length > 0;

  if (formStateReady) {
    if (!!formState.name.length  && !!formState.description.length && !canSubmit) {
      // The setTimeout stops a warning. Admittedly unsure why.
      setTimeout(() => {
        setCanSubmit(prev => true)
      })
    }
    if (canSubmit && (!formState.name.length || !formState.description.length)) {
      setCanSubmit(prev => false)
    }
  }

  return formStateReady && <>
    <Box>
      <label>Name</label>

      <TextField
        variant="outlined"
        value={formState.name}
        onInput={(e) => handleChange(e, 'name', setFormState)}
      />
    </Box>
    <Box>
      <label>Description</label>
      <TextField
        variant="outlined"
        value={formState.description}
        onInput={(e) => handleChange(e, 'description', setFormState)}
        multiline
        rows={4}
      />
    </Box>
    <details>
      <summary>Schema Json</summary>
      {schemaJson !==  null && <pre>!!!{JSON.stringify(schemaJson, null, 2)}</pre>}
    </details>
  </>

}

export default EditContentTypeForm;
