import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FormControl from '@mui/material/FormControl';
import {InputLabel, Select} from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';


const EditDisplayForm = () => {

  return <Box>
    <Typography htmlFor="group" id="modal-modal-title" variant="h6" component="h2">
      Label
    </Typography>
    <FormControl fullWidth>
      <InputLabel id="demo-simple-select-label">-Select label-</InputLabel>
      <Select label="-Select label-" value="" id="group">
        <MenuItem value="above">Above</MenuItem>
        <MenuItem value="inline">Inline</MenuItem>
        <MenuItem value="hidden">- Hidden -</MenuItem>
        <MenuItem value="visually-hidden">- Visually Hidden -</MenuItem>
      </Select>
    </FormControl>
    <Typography htmlFor="group" id="modal-modal-title" variant="h6" component="h2">
      Format
    </Typography>
    <FormControl fullWidth>
      <InputLabel id="demo-simple-select-label">-Select format-</InputLabel>
      <Select label="-Select a format-" value="" id="group">
        <MenuItem value="tabs">Placeholder</MenuItem>
        <MenuItem value="details-sidebar">Placeholder</MenuItem>
        <MenuItem value="details">Placeholder</MenuItem>
      </Select>
    </FormControl>
    <Button style={{marginTop: '1rem'}} variant="contained">Save</Button>
  </Box>
}

export default EditDisplayForm;
