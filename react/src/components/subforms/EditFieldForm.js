import {useEffect, useState} from "react";

const EditFieldForm = ({field }) => {
  const [schemaFieldJson, setSchemaFieldJson] = useState(null);
  useEffect(() => {
    Promise.all([`../jsonapi/field_config/field_config?filter[id]=${field.id}`].map(url =>
      fetch(url).then(resp => resp.json())
    )).then(jsons => {
      setSchemaFieldJson(jsons[0])
    })
  }, [])
  return <>
    <details>
      <summary>Field JSON</summary>
      <pre>{JSON.stringify(schemaFieldJson, null, 2)}</pre>
    </details>
  </>
}

export default EditFieldForm;
