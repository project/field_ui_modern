import {useEffect, useState} from "react";

const EditFieldStorageForm = ({field }) => {
  const [schemaStorageJson, setSchemaStorageJson] = useState(null);
  useEffect(() => {
    Promise.all([`../jsonapi/field_storage_config/field_storage_config?filter[drupal_internal__id]=${field.attributes.entity_type}.${field.attributes.field_name}`].map(url =>
      fetch(url).then(resp => resp.json())
    )).then(jsons => {
      setSchemaStorageJson(jsons[0])
    })
  }, [])
  return <>
    <details>
      <summary>Storage Schema JSON</summary>
      <pre>{JSON.stringify(schemaStorageJson, null, 2)}</pre>
    </details>
  </>
}

export default EditFieldStorageForm;
