import {useEffect, useState} from "react";
import {useSelector} from "react-redux";


const FormDisplayForm = ({contentType}) => {
  const [schemaJson, setSchemaJson] = useState(null);
  const fieldRelatedData = useSelector(state => state.fieldRelatedData.value)
  const widgetsJson = fieldRelatedData.widgets;

  useEffect(() => {
    Promise.all([`../jsonapi/entity_form_display/entity_form_display?filter[bundle]=${contentType.attributes.drupal_internal__type}`,
    ].map(url =>
      fetch(url).then(resp => resp.json())
    )).then(jsons => {
      setSchemaJson(jsons[0])
    })
  }, [])

  return schemaJson !== null && <>
    <details>
      <summary>Form Schema
      </summary>
      <pre>{JSON.stringify(schemaJson, null, 2)}</pre>

    </details>
    <details>
      <summary>Widgets JSON
      </summary>
      <pre>{JSON.stringify(widgetsJson, null, 2)}</pre>

    </details>
  </>
}

export default FormDisplayForm;
