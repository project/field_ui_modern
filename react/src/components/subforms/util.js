

export const handleChange = (e, property, formStateSetter, value = 'value') => {
  formStateSetter(prevState => {
    return {
      ...prevState,
      [property]: e.target[value],
    }
  })
}

export const getFieldTypesByCategory = (field_types) => {
  const fieldTypesByCategory = {};
  Object.entries(field_types).forEach(([name, value]) => {
    if (!fieldTypesByCategory[value.category]) {
      fieldTypesByCategory[value.category] = [];
    }
    if (value.no_ui === false) {
      fieldTypesByCategory[value.category].push(value);
    }
  })
  return fieldTypesByCategory;
}

export const getExistingSelectableFields = (contentState, fieldState) => {
  const existingFields = new Set();
  contentState.data.forEach(contentType => {
    fieldState[contentType.attributes.drupal_internal__type].data.forEach((field) => {
      existingFields.add(`${field.attributes.field_type}: ${field.attributes.field_name}`)
    })
  })
  return existingFields;
}
