import { configureStore } from '@reduxjs/toolkit'
import fieldReducer from './stores/fieldSlice'
import contentTypeReducer from './stores/contentTypeSlice'
import fieldRelatedReducer from './stores/fieldRelatedSlice'

export default configureStore({
  reducer: {
    fields: fieldReducer,
    contentTypes: contentTypeReducer,
    fieldRelatedData: fieldRelatedReducer,
  }
})
