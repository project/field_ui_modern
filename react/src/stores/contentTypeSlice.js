import { createSlice } from '@reduxjs/toolkit'

export const contentTypeSlice = createSlice({
  name: 'contentTypes',
  initialState: {
    value: {}
  },
  reducers: {
    set: (state, action) => {
      state.value = action.payload;
    },
    add: state => {

    },
    remove: (state, action) => {
      const newState = { ...state};
      newState.value.data = newState.value.data.filter((item) => item.id !== action.payload.id)
      state = newState;
    },
    patch: state => {

    },
  }
})

// Action creators are generated for each case reducer function
export const { set, add, remove, patch } = contentTypeSlice.actions

export default contentTypeSlice.reducer
