import { createSlice } from '@reduxjs/toolkit'


export const fieldRelatedSlice = createSlice({
  name: 'fieldRelatedData',
  initialState: {
    value: {}
  },
  reducers: {
    set: (state, action) => {
      state.value = action.payload;
    },
  }
})

export const { set } = fieldRelatedSlice.actions;

export default fieldRelatedSlice.reducer;
