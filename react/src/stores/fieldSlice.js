import { createSlice } from '@reduxjs/toolkit'

export const fieldsSlice = createSlice({
  name: 'fields',
  initialState: {
    value: {}
  },
  reducers: {
    set: (state, action) => {
      state.value = action.payload;
    },
    add: state => {

    },
    remove: state => {

    },
    patch: state => {

    },
  }
})

// Action creators are generated for each case reducer function
export const { set, add, remove, patch } = fieldsSlice.actions

export default fieldsSlice.reducer
