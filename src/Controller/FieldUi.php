<?php

namespace Drupal\field_ui_modern\Controller;

use Drupal\Core\Controller\ControllerBase;

class FieldUi extends ControllerBase{

 public function toAttach() {
   return [
     '#type' => 'container',
     '#attributes' => [
       'id' => 'root',
     ],
     '#markup' => 'If React was running, I would not be here',
     '#attached' => [
       'library' => 'field_ui_modern/client_dev',
     ]
   ];
 }
}
