<?php

namespace Drupal\field_ui_modern\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;

class Fields extends ControllerBase {
 public function types(): JsonResponse {
   $field_type_manager = \Drupal::service('plugin.manager.field.field_type');
   $defs =  $field_type_manager->getDefinitions();
   foreach( $defs as $name => &$definition) {
     $definition['default_storage_settings'] = $field_type_manager->getDefaultStorageSettings($name);
     $definition['default_field_settings'] = $field_type_manager->getDefaultFieldSettings($name);
     $definition['preconfigured_options'] = $field_type_manager->getPreconfiguredOptions($name);
   }
   return new JsonResponse($defs, 200);
 }
  public function widgets(): JsonResponse {
    $defs = \Drupal::service('plugin.manager.field.widget')->getDefinitions();
    return new JsonResponse($defs, 200);
  }

  public function formatters(): JsonResponse {
    $defs = \Drupal::service('plugin.manager.field.formatter')->getDefinitions();
    return new JsonResponse($defs, 200);
  }

  public function allFieldRelatedData(): JsonResponse {
    // Sure, this direct YAML parse is probably way hacky and can be achieved with
    // JSON API. This was an approach I knew how to do already so @todo lets make
    // it nicer eventually.
    $data_types = Yaml::parseFile('core/config/schema/core.data_types.schema.yml');
    $field_type_manager = \Drupal::service('plugin.manager.field.field_type');
    $field_type_definitions =  $field_type_manager->getDefinitions();
    foreach($field_type_definitions as $name => &$definition) {
      $definition['default_storage_settings'] = $field_type_manager->getDefaultStorageSettings($name);
      $definition['default_field_settings'] = $field_type_manager->getDefaultFieldSettings($name);
      $definition['preconfigured_options'] = $field_type_manager->getPreconfiguredOptions($name);
    }
   $all_defs = [
     'field_types' => $field_type_definitions,
     'data_types' => $data_types,
     'ui_definitions' => $field_type_manager->getUiDefinitions(),
     'widgets' => \Drupal::service('plugin.manager.field.widget')->getDefinitions(),
     'formatters' => \Drupal::service('plugin.manager.field.formatter')->getDefinitions(),
   ];
    return new JsonResponse($all_defs, 200);
  }

}
